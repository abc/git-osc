---
layout: post
title: "为什么你的项目在 Gitee 不容易被搜到？"
---

<p>10月份时，Gitee 的搜索进行了一次重构（<a href="https://www.oschina.net/news/110781/gitee-search-refine">详情</a>），因为之前的搜索效果太烂了了了了！！！！ 自从上线以来，负责优化该功能的&nbsp;&nbsp;<a href="https://www.oschina.net/p/jcseg">#jcseg#</a>&nbsp;的作者&nbsp;<a href="https://my.oschina.net/jcseg">@狮子的魂</a>&nbsp;可没少被吐槽啊。</p>

<p>然后我们又做了第二次的重构，这一次动作有点大，差点扯到了&nbsp;<a href="https://my.oschina.net/jcseg">@狮子的魂</a>&nbsp;的淡淡。</p>

<p>作为一个技术人，知道大家更关心实际的搜索技术细节，所以我就把底裤扒出来给你看！</p>

<p>主要有几点：</p>

<p><strong>1. 使用独立二级域名 search.gitee.com ，使用宇宙最好编程语言&nbsp;PHP 替代 Ruby 实现搜索入口</strong></p>

<p>你别说，速度好像还挺快的。</p>

<p><strong>2. 提供搜索命中的细节，如下图所示（是不是有点点装逼逼？）</strong></p>

<p>现在你总算知道为什么你的项目搜不到了吧？</p>

<p><img src="https://static.oschina.net/uploads/space/2020/0103/120414_eqpU_12.png" width="600" /></p>

<p><strong>3. 增加搜索关键词对应的一些优秀技术文章（来自 OSChina 社区）</strong></p>

<p><img src="https://static.oschina.net/uploads/space/2020/0103/120525_hIMF_12.png" width="400" /></p>

<p><strong>4. 搜索细节的优化，目的是让搜索更准确，排序更合理</strong></p>

<p><strong>-----------------</strong></p>

<p>没啦，接下来你可以到&nbsp;<a href="https://gitee.com/explore">https://gitee.com/explore</a> 开始搜索，然后吐槽。</p>

<p>请轻点吐槽，因为&nbsp;<a href="https://my.oschina.net/jcseg">@狮子的魂</a>&nbsp; 最近有点抑郁了，小心他死给你看！</p>

<p>最后奉上福利&nbsp;<a href="https://gitee.com/oschina/gitee-search-share">https://gitee.com/oschina/gitee-search-share</a>&nbsp; 深入讲解 Gitee 的搜索细节 PPT 以及我们的词库。</p>
