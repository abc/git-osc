---
layout: post
title: "不见面，仍在线"
---

<p>面对来势汹汹的&ldquo;新型冠状病毒肺炎&rdquo;，多个省份启动重大突发公共卫生事件一级响应。春节假期延长，不少企业也采取了延迟上班、远程上班等应对措施。</p>

<p>对开发者这样的脑力劳动者而言，只要电脑在手，身在何处都不影响我们搬砖。那么问题来了：如何更高效地协同？团队如何迅速适应远程协作？</p>

<p><strong>Gitee 企业版提供安全、稳定、可靠的企业级项目管理和代码协作一体化云开发平台</strong>，即使我们相隔万里，依然可以通过它进行实现团队研发工作的规划、拆解、跟踪，以及便利的编码协同。</p>

<p><img height="601" src="https://static.oschina.net/uploads/space/2020/0130/190416_eVdP_3820517.png" width="700" /></p>

<h2>精细代码管理</h2>

<ul>
	<li>统一管理代码资源，方便随时随地访问</li>
	<li>精细权限控制，可对分支、文件单独设置权限</li>
	<li>完整、清晰、可视化地记录代码变更过程</li>
</ul>

<p>Gitee 企业版的「 Pull Request 」可在开发者提交代码后，自动触发代码质量分析，减少人工审核，提升效率。同时支持在线代码评审，帮助企业提升代码质量。</p>

<h2>过程管理</h2>

<p>Gitee 企业版不仅精于代码管理，亦可灵活支撑团队研发的过程管理：</p>

<p>产品经理（或其他成员）使用码「需求管理」提出「需求」；</p>

<p>需求经确认可纳入「项目」管理，由技术管理者转化成技术实现方案，通过「里程碑」规划迭代；</p>

<p>接下来，将里程碑拆分为具体任务，安排给技术团队成员，实现从需求产生到落地的管理。&nbsp; &nbsp; &nbsp;&nbsp;</p>

<h2>可视化</h2>

<ul>
	<li>看板（支持类型、状态、成员三种视角）</li>
	<li>甘特图</li>
	<li>燃尽图</li>
</ul>

<h2>即时通知</h2>

<p>支持短信、微信、邮件即时通知&nbsp;</p>

<h2>试试看？</h2>

<p>Gitee 企业版还提供 CI/CD 集成，测试缺陷管理等等企业级代码拓展服务。</p>

<p>已帮助超过 10 万家企业提升研发效能</p>

<p><a href="https://gitee.com/enterprises" target="_blank">&gt;&gt; 免费开通&nbsp;Gitee&nbsp;企业版&nbsp;</a></p>

<p><a href="https://gitee.com/enterprises" target="_blank">https://gitee.com/enterprises</a></p>

<p>专业的企业级代码协作管理平台</p>

<h2>一份微薄之力</h2>

<p>Gitee 向奋战在疫情防控一线的医疗机构/公益组织/政府部门等提供免费的企业版产品和技术支持服务，助力相关研发工作的高效开展、共克时艰。</p>

<p>如有需要，请在 Gitee 微信服务号留言&ldquo;疫情防控&rdquo;</p>

<p><img height="121" src="https://static.oschina.net/uploads/space/2020/0130/190510_wXHL_3820517.png" width="120" /></p>

<p>扫码关注 Gitee</p>

<h1 style="text-align:center">病毒无情，但我们终将战胜！</h1>
